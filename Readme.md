# Introduction
This test automation framework is developed by Akshay Goel for the assignment given by HelloFresh, right from almost the scratch (revived from the very basic project provided by HelloFresh)

# Features
1.  Running Test cases in sequential/parallel mode
2.  Parallel mode can be configured automatically by framework, you dont have to start hub and nodes individually, its been taken care by framework. Also, if you want to keep manual control, configuration for auto configure can be turned off
3.  Extent reporting has been introduced to show feature file and scenario wise data. It also provides step level status alongwith the exceptions in case of failures.
4.  High level data tab is also there on the report which talk about the number of scenarios and steps executed/passed/failed/skipped etc and that too with Graphical pie charts as well
5.  Log report is also there which gives intensive logs alongwith the links of screenshots in case of failures
6.  Screnshot folder provides all the captures during the failures while running Test cases
7.  Intensive logging can be seen both on the execution window of command prompt/console as well as log file
8.  Framework has been structed with different logical packages and classes
9.  Code has been divided into different layers to provide flexibility of reusing individual components whenever required and at the same time providing easy to understand/write test cases
10. Support for different browsers is provided in the framework, download the binary and change the configuration. (Specific capabilities code is still in TODO but will give you the idea behind the implementation)
11. Rest API test functionality is supported and the internal classes are being developed with partially (due to time limitation) DTO based structure
12. Object Repository and cache file automatic handling as well as buttery smooth integration with feature file writing gives out of the world rich experience
13. A lot others I can talk about if we meet and you are interested in it...

# How To
This section talks about how to run this framework:

Answer is simple: 
1.  Download the code (which I think you have done already, if you are reading this file locally)
2.  Double click on the "run.bat" file 
(Optional) if you want to run on different machines use the hub and node files provided

OR 

1. Import project in Eclipse (or any other IDE of your choice)
2. Find 'src\test\java\com\hellofresh\runner\FrameworkStart.java' and run it as Junit Runner

## For your Tinkering buds

1. Find 'src\test\resources\config.yml' there are a number of configurations I have exposed, which can be used to control framework behavior
2. Find 'src\test\resources\ObjectRepository\' this folder contains your object repositories and you can choose which one to use as per your different test env's and product releases
3. Find 'src\test\resources\TestCases\' put all your feature files here to execute (no need to mention it anywhere)
4. Find 'src\test\resources\Cache\' put all your TestData files here and your framework will pick those files for usage
5. Find 'src\test\resources\hub.bat' and 'src\test\resources\node.bat' framework creates these files on the fly, so dont worry if you accidentally deleted them, framework will create those and configures your grid automatically

# Test Points coverage
All the mentioned test points in the Tasks 1 and 2 have been covered in the 2 feature files (named accordingly)

## Additional features coverage:
| Task   | Feature Name                | Status   | Comments                                                                                |
| ------ | --------------------------- | -------- | --------------------------------------------------------------------------------------- |
| Task 1 | Logging                     | Done     | File, console and Report                                                                |
|        | Screenshot on failed test   | Done     | Logging report link to screenshot and Physical file in screenshot folder                |
|        | Generate Report             | Done     | Extent Report, logs, cucumber based Json reports are getting generated                  |
|        | Generating random test data | Done     | Random data generater functions provided to testcase writter (already used)             |
|        | Web Driver factory          | Progress | Implemented conceptually half way, but scope of improvement is there                    |
|        | Encapsulate                 | Progress | Done with necessary levels, but can be improved                                         |
|        | Configurator                | Progress | No. of configurations provided, but that can be centralised with Configurator           |
|        | Parallel run                | Done     | Implemented with Remote Driver and surefire plugin                                      |
|        | Different browser           | Progress | Configuration provided, implementation in progress for other than Chrome                |
|        | Different Env               | Done     | Configuration and object repo handles this                                              |
|        | Test Data from File         | Done     | Data in Scenario Outlines & ObjectRepo handles runtime data based identifier generation |
| Task 2 | Get Bookings and Count      | Done     | provision provided for data and count verification                                      |
|        | Get Bookings                | Done     | Data verification is also done                                                          |
|        | Create Booking              | Done     | Data generation by user preference & conditions taken care                              |
|        |                             |          | Provision provided for verification of Various conditions                               |
|        | Update Booking              | Progress | Data creation and put request done but working with cookies for 403 forbidden response  |
|        | Delete Booking              | Unbegan  | Provision provided to implement easily                                                  |

# Reports
1.  Extent Report - "Reports\ExtentHtml.html" gets generated after every run
2.  Screenshots - "Reports\Screenshots\" this folder contains all the screenshot on failures
3.  Logs - "Reports\index.html" and "Reports\Logs.log" contains all the logs of the execution
4.  Dashboard - "Reports\Dashboard.html" a high level view of execution including the timestamps

# Disclaimer & reuse
Please note that all of the code written in this framework is written from scratch (and in original) by Akshay Goel.
Although you are free to reuse/extend this, it would be advisable to give proper recognition to Akshay whenever you are publishing your work which is using anything from my work.

# Contact 
If you need any other information/support, feel free to give me a buzz:

Email: akshay_goel@live.in
Phone: +91-8586807016
Linkedin: https://www.linkedin.com/in/akshay-goel-b81a7122/
