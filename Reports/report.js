$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/TestCases/APITest.feature");
formatter.feature({
  "name": "HelloFresh Task-2",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Create and Get Bookings",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User perform operation \"stringConcat\" on \"\u0027config:ApiBaseURL\u0027\" and \"/\" then save results in \"URL\"",
  "keyword": "When "
});
formatter.match({
  "location": "DataAndValues.performOperation(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User generates data for action \"RandomNumeric\" with value \"5\" and save to \"prefixer\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.stringGeneration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User generates data for action \"RandomNumeric\" with value \"2\" and save to \"bookingid\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.stringGeneration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User generates data for action \"RandomNumeric\" with value \"5\" and save to \"roomid\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.stringGeneration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User generates data for action \"getDate\" with value \"yyyy-MM-dd\" and save to \"checkin\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.stringGeneration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User generates data for action \"dateadddays\" with value \"5\" and save to \"checkout\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.stringGeneration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User perform operation \"stringConcat\" on \"mystyle\u0027cache:prefixer\u0027\" and \"@aky\u0027cache:prefixer\u0027.com\" then save results in \"email\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.performOperation(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User generates data for action \"UniqueString\" with value \"First:4\" and save to \"firstname\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.stringGeneration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User generates data for action \"UniqueString\" with value \"Last:5\" and save to \"lastname\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.stringGeneration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User generates data for action \"RandomNumeric\" with value \"12\" and save to \"phone\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.stringGeneration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User calls Rest API \"\u0027cache:URL\u0027\" with \"post\" method with Create Booking data and save response as \"createBooking.json\"",
  "rows": [
    {
      "cells": [
        "checkin",
        "\u0027cache:checkin\u0027"
      ]
    },
    {
      "cells": [
        "checkout",
        "\u0027cache:checkout\u0027"
      ]
    },
    {
      "cells": [
        "bookingid",
        "\u0027cache:bookingid\u0027"
      ]
    },
    {
      "cells": [
        "depositpaid",
        "true"
      ]
    },
    {
      "cells": [
        "email",
        "\u0027cache:email\u0027"
      ]
    },
    {
      "cells": [
        "firstname",
        "\u0027cache:firstname\u0027"
      ]
    },
    {
      "cells": [
        "lastname",
        "\u0027cache:lastname\u0027"
      ]
    },
    {
      "cells": [
        "phone",
        "\u0027cache:phone\u0027"
      ]
    },
    {
      "cells": [
        "roomid",
        "\u0027cache:roomid\u0027"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "APICall.callAPIWithDataSaveResponse(String,String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User calls Rest API \"\u0027cache:URL\u0027\" with \"get\" method and save response as \"getBookings.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "APICall.callAPISaveResponse(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User validates Get Bookings Json response \"\u0027cachefile:getbookings.json\u0027\" having criteria",
  "rows": [
    {
      "cells": [
        "count",
        "\u003e1"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "APICall.validateJsonWithDataTable(String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User saves booking id from CreatBookingResponse Json \"\u0027cachefile:createBooking.json\u0027\" as \"createdBookingId\"",
  "keyword": "When "
});
formatter.match({
  "location": "APICall.saveValueFromJson(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User perform operation \"stringConcat\" on \"\u0027cache:URL\u0027\" and \"\u0027cache:createdBookingId\u0027\" then save results in \"GetBookingURL\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.performOperation(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User calls Rest API \"\u0027cache:GetBookingURL\u0027\" with \"get\" method and save response as \"getBooking.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "APICall.callAPISaveResponse(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User validates Get Booking Json response \"\u0027cachefile:getbooking.json\u0027\" having criteria",
  "rows": [
    {
      "cells": [
        "checkin",
        "\u0027cache:checkin\u0027"
      ]
    },
    {
      "cells": [
        "checkout",
        "\u0027cache:checkout\u0027"
      ]
    },
    {
      "cells": [
        "bookingid",
        "\u0027cache:createdBookingId\u0027"
      ]
    },
    {
      "cells": [
        "depositpaid",
        "true"
      ]
    },
    {
      "cells": [
        "firstname",
        "\u0027cache:firstname\u0027"
      ]
    },
    {
      "cells": [
        "lastname",
        "\u0027cache:lastname\u0027"
      ]
    },
    {
      "cells": [
        "roomid",
        "\u0027cache:roomid\u0027"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "APICall.validateBookingJsonWithDataTable(String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User perform operation \"stringConcat\" on \"\u0027cache:firstname\u0027\" and \"_updated\" then save results in \"updFName\"",
  "keyword": "When "
});
formatter.match({
  "location": "DataAndValues.performOperation(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User perform operation \"stringConcat\" on \"\u0027cache:lastname\u0027\" and \"_updated\" then save results in \"updLName\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.performOperation(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User perform operation \"stringConcat\" on \"\u0027cache:roomid\u0027\" and \"0\" then save results in \"updroom\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.performOperation(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("src/test/resources/TestCases/WebTest.feature");
formatter.feature({
  "name": "HelloFresh Task-1",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "TestCase-1 Sign In",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User navigates to \"\u0027config:AppBasicURL\u0027\"",
  "keyword": "When "
});
formatter.step({
  "name": "User clicks on \"\u0027or:HomePage.SignIn\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User generates data for action \"getDateTime\" with value \"\" and save to \"timestamp\"",
  "keyword": "Given "
});
formatter.step({
  "name": "User perform operation \"stringConcat\" on \"hf_challenge_\u0027cache:timestamp\u0027\" and \"@hf\u0027cache:timestamp\u0027.com\" then save results in \"email\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u0027cache:email\u0027\" in \"\u0027or:Login.SignUp_Email\u0027\"",
  "keyword": "When "
});
formatter.step({
  "name": "User clicks on \"\u0027or:Login.SignUp_Submit\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:SignUp.Gender\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cfName\u003e\" in \"\u0027or:SignUp.FirstName\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003clName\u003e\" in \"\u0027or:SignUp.LastName\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cpwd\u003e\" in \"\u0027or:SignUp.Password\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User selects \"Value\" \u003d \"\u003cday\u003e\" from dropdown \"\u0027or:SignUp.DOB_Days\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User selects \"Value\" \u003d \"\u003cmonth\u003e\" from dropdown \"\u0027or:SignUp.DOB_Month\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User selects \"Value\" \u003d \"\u003cyear\u003e\" from dropdown \"\u0027or:SignUp.DOB_Year\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003ccmpny\u003e\" in \"\u0027or:SignUp.Company\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cadd1\u003e\" in \"\u0027or:SignUp.Address_Line1\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cadd2\u003e\" in \"\u0027or:SignUp.Address_Line2\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003ccity\u003e\" in \"\u0027or:SignUp.City\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User selects \"Text\" \u003d \"\u003cstate\u003e\" from dropdown \"\u0027or:SignUp.State\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cpostcode\u003e\" in \"\u0027or:SignUp.ZipCode\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cotherinfo\u003e\" in \"\u0027or:SignUp.AdditionalInfo\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cphone\u003e\" in \"\u0027or:SignUp.HomePhone\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cmobile\u003e\" in \"\u0027or:SignUp.MobilePhone\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003calias\u003e\" in \"\u0027or:SignUp.Alias\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:SignUp.Submit\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.Heading\u0027\" and save it as \"heading\"",
  "keyword": "And "
});
formatter.step({
  "name": "User verifies \"\u0027cache:heading\u0027\" \"equal\" \"MY ACCOUNT\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.Name\u0027\" and save it as \"accountName\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:accountName\u0027\" \"equal\" \"\u003cfName\u003e \u003clName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.WelcomeMessage\u0027\" and save it as \"welcomeMessage\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:welcomeMessage\u0027\" \"contain\" \"Welcome to your account.\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User fetch property \"Displayed\" of Web-Control \"\u0027or:Account.Logout\u0027\" and save it as \"logoutDisplayed\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:logoutDisplayed\u0027\" is \"true\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User save current URL as \"userAccountPageURL\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:userAccountPageURL\u0027\" \"contain\" \"controller\u003dmy-account\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "fName",
        "lName",
        "pwd",
        "day",
        "month",
        "year",
        "cmpny",
        "add1",
        "add2",
        "city",
        "state",
        "postcode",
        "otherinfo",
        "phone",
        "mobile",
        "alias"
      ]
    },
    {
      "cells": [
        "FirstName",
        "LastName",
        "Qwerty",
        "1",
        "1",
        "2000",
        "Company",
        "Qwerty, 123",
        "zxcvb",
        "Qwerty",
        "Colorado",
        "12345",
        "Qwerty",
        "12345123123",
        "12345123123",
        "hf"
      ]
    }
  ]
});
formatter.scenario({
  "name": "TestCase-1 Sign In",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User navigates to \"\u0027config:AppBasicURL\u0027\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.navigation(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:HomePage.SignIn\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "error_message": "org.openqa.selenium.NoSuchSessionException: invalid session id\nBuild info: version: \u00273.11.0\u0027, revision: \u0027e59cfb3\u0027, time: \u00272018-03-11T20:26:55.152Z\u0027\nSystem info: host: \u0027OPTIPLEX\u0027, ip: \u0027192.168.0.107\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_191\u0027\nDriver info: org.openqa.selenium.remote.RemoteWebDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 78.0.3904.97, chrome: {chromedriverVersion: 78.0.3904.70 (edb9c9f3de024..., userDataDir: C:\\Users\\aksha\\AppData\\Loca...}, goog:chromeOptions: {debuggerAddress: localhost:55199}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webdriver.remote.sessionid: a265b7f3e55ae31a8b00c9f0bfd...}\n*** Element info: {Using\u003dclass name, value\u003dlogin}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:545)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:319)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByClassName(RemoteWebDriver.java:405)\r\n\tat org.openqa.selenium.By$ByClassName.findElement(By.java:391)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:311)\r\n\tat com.hellofresh.steps.WebDriverOperations.getElementWhenVisible(WebDriverOperations.java:113)\r\n\tat com.hellofresh.steps.WebDriverOperations.click(WebDriverOperations.java:46)\r\n\tat ✽.User clicks on \"\u0027or:HomePage.SignIn\u0027\"(src/test/resources/TestCases/WebTest.feature:6)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "User generates data for action \"getDateTime\" with value \"\" and save to \"timestamp\"",
  "keyword": "Given "
});
formatter.match({
  "location": "DataAndValues.stringGeneration(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User perform operation \"stringConcat\" on \"hf_challenge_\u0027cache:timestamp\u0027\" and \"@hf\u0027cache:timestamp\u0027.com\" then save results in \"email\"",
  "keyword": "And "
});
formatter.match({
  "location": "DataAndValues.performOperation(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"\u0027cache:email\u0027\" in \"\u0027or:Login.SignUp_Email\u0027\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User clicks on \"\u0027or:Login.SignUp_Submit\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User clicks on \"\u0027or:SignUp.Gender\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"FirstName\" in \"\u0027or:SignUp.FirstName\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"LastName\" in \"\u0027or:SignUp.LastName\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"Qwerty\" in \"\u0027or:SignUp.Password\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User selects \"Value\" \u003d \"1\" from dropdown \"\u0027or:SignUp.DOB_Days\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.selectDropDown(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User selects \"Value\" \u003d \"1\" from dropdown \"\u0027or:SignUp.DOB_Month\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.selectDropDown(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User selects \"Value\" \u003d \"2000\" from dropdown \"\u0027or:SignUp.DOB_Year\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.selectDropDown(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"Company\" in \"\u0027or:SignUp.Company\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"Qwerty, 123\" in \"\u0027or:SignUp.Address_Line1\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"zxcvb\" in \"\u0027or:SignUp.Address_Line2\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"Qwerty\" in \"\u0027or:SignUp.City\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User selects \"Text\" \u003d \"Colorado\" from dropdown \"\u0027or:SignUp.State\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.selectDropDown(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"12345\" in \"\u0027or:SignUp.ZipCode\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"Qwerty\" in \"\u0027or:SignUp.AdditionalInfo\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"12345123123\" in \"\u0027or:SignUp.HomePhone\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"12345123123\" in \"\u0027or:SignUp.MobilePhone\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters text \"hf\" in \"\u0027or:SignUp.Alias\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User clicks on \"\u0027or:SignUp.Submit\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.Heading\u0027\" and save it as \"heading\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verifies \"\u0027cache:heading\u0027\" \"equal\" \"MY ACCOUNT\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.Name\u0027\" and save it as \"accountName\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verifies \"\u0027cache:accountName\u0027\" \"equal\" \"FirstName LastName\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.WelcomeMessage\u0027\" and save it as \"welcomeMessage\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verifies \"\u0027cache:welcomeMessage\u0027\" \"contain\" \"Welcome to your account.\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User fetch property \"Displayed\" of Web-Control \"\u0027or:Account.Logout\u0027\" and save it as \"logoutDisplayed\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verifies \"\u0027cache:logoutDisplayed\u0027\" is \"true\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyTrueFalse(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User save current URL as \"userAccountPageURL\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.saveCurrentURL(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verifies \"\u0027cache:userAccountPageURL\u0027\" \"contain\" \"controller\u003dmy-account\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "error_message": "java.lang.NullPointerException\r\n\tat com.hellofresh.runner.Pre_requisite.ScenarioCleanup(Pre_requisite.java:33)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:26)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:20)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runner.HookDefinitionMatch.runStep(HookDefinitionMatch.java:16)\r\n\tat cucumber.runner.TestStep.executeStep(TestStep.java:63)\r\n\tat cucumber.runner.TestStep.run(TestStep.java:49)\r\n\tat cucumber.runner.TestCase.run(TestCase.java:48)\r\n\tat cucumber.runner.Runner.runPickle(Runner.java:40)\r\n\tat cucumber.runtime.junit.PickleRunners$NoStepDescriptions.run(PickleRunners.java:146)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:68)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:23)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:73)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:124)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:65)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.apache.maven.surefire.junitcore.pc.Scheduler$1.run(Scheduler.java:387)\r\n\tat java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)\r\n\tat java.util.concurrent.FutureTask.run(FutureTask.java:266)\r\n\tat java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)\r\n\tat java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)\r\n\tat java.lang.Thread.run(Thread.java:748)\r\n",
  "status": "failed"
});
formatter.scenarioOutline({
  "name": "TestCase-2 Log In",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User navigates to \"\u0027config:AppBasicURL\u0027\"",
  "keyword": "When "
});
formatter.step({
  "name": "User clicks on \"\u0027or:HomePage.SignIn\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cemail\u003e\" in \"\u0027or:Login.SignIn_Email\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cpwd\u003e\" in \"\u0027or:Login.SignIn_Pwd\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:Login.SignIn_Submit\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.Heading\u0027\" and save it as \"heading\"",
  "keyword": "And "
});
formatter.step({
  "name": "User verifies \"\u0027cache:heading\u0027\" \"equal\" \"MY ACCOUNT\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.Name\u0027\" and save it as \"accountName\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:accountName\u0027\" \"equal\" \"\u003cUserName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.WelcomeMessage\u0027\" and save it as \"welcomeMessage\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:welcomeMessage\u0027\" \"contain\" \"Welcome to your account.\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User fetch property \"Displayed\" of Web-Control \"\u0027or:Account.Logout\u0027\" and save it as \"logoutDisplayed\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:logoutDisplayed\u0027\" is \"true\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User save current URL as \"userAccountPageURL\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:userAccountPageURL\u0027\" \"contain\" \"controller\u003dmy-account\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "email",
        "pwd",
        "UserName"
      ]
    },
    {
      "cells": [
        "hf_challenge_123456@hf123456.com",
        "12345678",
        "Joe Black"
      ]
    }
  ]
});
formatter.scenario({
  "name": "TestCase-2 Log In",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User navigates to \"\u0027config:AppBasicURL\u0027\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.navigation(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:HomePage.SignIn\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters text \"hf_challenge_123456@hf123456.com\" in \"\u0027or:Login.SignIn_Email\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters text \"12345678\" in \"\u0027or:Login.SignIn_Pwd\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:Login.SignIn_Submit\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.Heading\u0027\" and save it as \"heading\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:heading\u0027\" \"equal\" \"MY ACCOUNT\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.Name\u0027\" and save it as \"accountName\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:accountName\u0027\" \"equal\" \"Joe Black\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:Account.WelcomeMessage\u0027\" and save it as \"welcomeMessage\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:welcomeMessage\u0027\" \"contain\" \"Welcome to your account.\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fetch property \"Displayed\" of Web-Control \"\u0027or:Account.Logout\u0027\" and save it as \"logoutDisplayed\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:logoutDisplayed\u0027\" is \"true\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyTrueFalse(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User save current URL as \"userAccountPageURL\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.saveCurrentURL(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:userAccountPageURL\u0027\" \"contain\" \"controller\u003dmy-account\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "TestCase-3 Checkout",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User navigates to \"\u0027config:AppBasicURL\u0027\"",
  "keyword": "When "
});
formatter.step({
  "name": "User clicks on \"\u0027or:HomePage.SignIn\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cemail\u003e\" in \"\u0027or:Login.SignIn_Email\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters text \"\u003cpwd\u003e\" in \"\u0027or:Login.SignIn_Pwd\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:Login.SignIn_Submit\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:Account.WomenSectionLink\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:WomenSection.FadedShortSleeveTShirt\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User sleeps for \"5\" seconds",
  "keyword": "And "
});
formatter.step({
  "name": "User switch to frame \"\u0027or:WomenSection.ItemFrame\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:WomenSection.FadedShortSleeveTShirt_bigpic\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User switch to frame \"default\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:FadedShortSleeveTShirt.AddToCart\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:FadedShortSleeveTShirt.ProceedChckOut\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:CartSummary.ProceedChckOut\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:CartAddress.ProceedChckOut\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:CartShipping.TermsCondition\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:CartShipping.ProceedChckOut\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:Payment.BankWire\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User clicks on \"\u0027or:OrderSummary.Confirm\u0027\"",
  "keyword": "And "
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:OrderConfirmation.Heading\u0027\" and save it as \"OrdCnfHead\"",
  "keyword": "And "
});
formatter.step({
  "name": "User verifies \"\u0027cache:OrdCnfHead\u0027\" \"equal\" \"ORDER CONFIRMATION\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User fetch property \"Displayed\" of Web-Control \"\u0027or:OrderConfirmation.ShippingTab\u0027\" and save it as \"ShipTab\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:ShipTab\u0027\" is \"true\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User fetch property \"Displayed\" of Web-Control \"\u0027or:OrderConfirmation.PaymentTab\u0027\" and save it as \"PayTab\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:PayTab\u0027\" is \"true\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:OrderConfirmation.OrderCompleteMsg\u0027\" and save it as \"OrdCmpltMsg\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:OrdCmpltMsg\u0027\" \"contain\" \"Your order on My Store is complete.\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User save current URL as \"userAccountPageURL\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies \"\u0027cache:userAccountPageURL\u0027\" \"contain\" \"controller\u003dorder-confirmation\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "email",
        "pwd"
      ]
    },
    {
      "cells": [
        "hf_challenge_123456@hf123456.com",
        "12345678"
      ]
    }
  ]
});
formatter.scenario({
  "name": "TestCase-3 Checkout",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User navigates to \"\u0027config:AppBasicURL\u0027\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.navigation(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:HomePage.SignIn\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters text \"hf_challenge_123456@hf123456.com\" in \"\u0027or:Login.SignIn_Email\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters text \"12345678\" in \"\u0027or:Login.SignIn_Pwd\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.enterText(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:Login.SignIn_Submit\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:Account.WomenSectionLink\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:WomenSection.FadedShortSleeveTShirt\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sleeps for \"5\" seconds",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.userSleep(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User switch to frame \"\u0027or:WomenSection.ItemFrame\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.switchFrame(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:WomenSection.FadedShortSleeveTShirt_bigpic\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User switch to frame \"default\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.switchFrame(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:FadedShortSleeveTShirt.AddToCart\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:FadedShortSleeveTShirt.ProceedChckOut\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:CartSummary.ProceedChckOut\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:CartAddress.ProceedChckOut\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:CartShipping.TermsCondition\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:CartShipping.ProceedChckOut\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:Payment.BankWire\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"\u0027or:OrderSummary.Confirm\u0027\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.click(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:OrderConfirmation.Heading\u0027\" and save it as \"OrdCnfHead\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:OrdCnfHead\u0027\" \"equal\" \"ORDER CONFIRMATION\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fetch property \"Displayed\" of Web-Control \"\u0027or:OrderConfirmation.ShippingTab\u0027\" and save it as \"ShipTab\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:ShipTab\u0027\" is \"true\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyTrueFalse(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fetch property \"Displayed\" of Web-Control \"\u0027or:OrderConfirmation.PaymentTab\u0027\" and save it as \"PayTab\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:PayTab\u0027\" is \"true\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyTrueFalse(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fetch property \"Text\" of Web-Control \"\u0027or:OrderConfirmation.OrderCompleteMsg\u0027\" and save it as \"OrdCmpltMsg\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.getAndSaveProperty(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:OrdCmpltMsg\u0027\" \"contain\" \"Your order on My Store is complete.\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User save current URL as \"userAccountPageURL\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebDriverOperations.saveCurrentURL(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies \"\u0027cache:userAccountPageURL\u0027\" \"contain\" \"controller\u003dorder-confirmation\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DataAndValues.verifyEqualsOrContains(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});