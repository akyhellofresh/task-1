package com.hellofresh.api.dto;

import java.text.SimpleDateFormat;

import com.hellofresh.commonlibrary.Tokenizer;

public class GetBooking {
	public int bookingid;
    public int roomid;
    public String firstname;
    public String lastname;
    public boolean depositpaid;
    public BookingDates bookingdates;
    
    public boolean validate(String field, String value) throws Exception{
    	value = Tokenizer.getTokenizer(value).getValue();
    	boolean result = false;
    	switch(field.toLowerCase()) {
    	case "bookingid":	if(Integer.parseInt(value) == bookingid)	result = true;    	break;
    	case "roomid":		if(Integer.parseInt(value) == roomid)	result = true;    		break;
    	case "firstname":	if(firstname.equalsIgnoreCase(value))	result = true;			break;
    	case "lastname":	if(lastname.equalsIgnoreCase(value))	result = true;			break;
    	case "depositpaid":	if(Boolean.valueOf(value) == depositpaid)	result = true;		break;
    	case "checkin":		result = validateDates(bookingdates.checkin,value);   		break;
    	case "checkout":	result = validateDates(bookingdates.checkout,value);   		break;
    	}
    	return result;
    }
    
    private boolean validateDates(String objField, String userInput) throws Exception{
    	boolean result = false;
    	if(userInput.length() == 10) {
			new SimpleDateFormat("yyyy-MM-dd").parse(userInput);
		}else if(userInput.length() == 24) {
			userInput = userInput.substring(0,11);
			new SimpleDateFormat("yyyy-MM-dd").parse(userInput);
		}else {
			throw new Exception("Invalid Date time format : '" + userInput + "'");
		}
    	
    	if(objField.equalsIgnoreCase(userInput)) 	
    		result = true;
    		
    	return result;
    }
}
