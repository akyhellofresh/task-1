package com.hellofresh.api.dto;

import java.text.SimpleDateFormat;
import java.util.List;

import com.hellofresh.commonlibrary.Tokenizer;

import io.cucumber.datatable.DataTable;

public class CreateBooking {

	public BookingDates bookingdates;
	public int bookingid;
	public boolean depositpaid;
	public String email;
	public String firstname;
	public String lastname;
	public String phone;
	public int roomid;
	
	public CreateBooking(DataTable data) throws Exception{
		bookingdates = new BookingDates();
		List<List<String>> fields = data.asLists();
		for(List<String> field : fields) {
			String temp = Tokenizer.getTokenizer(field.get(1)).getValue();
			switch(field.get(0).toLowerCase()) {
	    	case "bookingid": bookingid = Integer.parseInt(temp); break;	
	    	case "roomid":	  roomid = Integer.parseInt(temp); break;
	    	case "firstname": firstname = temp; break;
	    	case "lastname":  lastname = temp; break;	
	    	case "depositpaid":	depositpaid = Boolean.valueOf(temp); break;
	    	case "checkin":	 	bookingdates.checkin = parseDate(temp);	break;	
	    	case "checkout": 	bookingdates.checkout = parseDate(temp);	break;	
	    	case "email":    email = temp; break;
	    	case "phone":    phone = temp; break;
	    	}
		}
	}
	
	private String parseDate(String rawString) throws Exception{
		String result = null;
		if(rawString.length() == 10) {
			new SimpleDateFormat("yyyy-MM-dd").parse(rawString);
			result = rawString;
		}else if(rawString.length() == 24) {
			new SimpleDateFormat("yyyy-MM-dd").parse(rawString.substring(0,11));
			result = rawString.substring(0,11);
		}else {
			throw new Exception("Invalid Date time format : '" + rawString + "'");
		}
		return result;
	}
}
