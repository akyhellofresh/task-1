package com.hellofresh.steps;

import static com.hellofresh.runner.FrameworkStart.CacheFolder;
import static com.hellofresh.runner.FrameworkStart.cacheFile;
import static com.hellofresh.runner.FrameworkStart.log;
import static io.restassured.RestAssured.get;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.google.gson.Gson;
import com.hellofresh.api.dto.CreateBooking;
import com.hellofresh.api.dto.CreateBookingResponse;
import com.hellofresh.api.dto.GetBooking;
import com.hellofresh.api.dto.GetBookings;
import com.hellofresh.commonlibrary.FileHandlr;
import com.hellofresh.commonlibrary.Tokenizer;

import cucumber.api.java.en.Given;
import io.cucumber.datatable.DataTable;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APICall {

	@Given("^User calls Rest API \"(.*?)\" with \"(.*?)\" method and save response as \"(.*?)\"$")
	public void callAPISaveResponse(String URL, String method, String filename) throws Exception{
		log.log(Level.INFO, "Entering APICall.callAPISaveResponse");
		Response resp = null;
		URL = Tokenizer.getTokenizer(URL).getValue();
		filename = Tokenizer.getTokenizer(filename).getValue();
		
		switch(method.toLowerCase()) {
		case "get":
			resp = get(URL);
			break;
		}
		FileHandlr.writeFile(true, CacheFolder, filename, resp.asString());
		log.log(Level.INFO, "Exiting APICall.callAPISaveResponse");
	}
	
	@Given("^User validates Get Bookings Json response \"(.*?)\" having criteria$")
	public void validateJsonWithDataTable(String Json, DataTable criteria) throws Exception{
		log.log(Level.INFO, "Entering APICall.validateJsonWithDataTable");
		Json = Tokenizer.getTokenizer(Json).getValue();
		
		Gson jr = new Gson();
		GetBooking[] booking = jr.fromJson(Json, GetBookings.class).bookings;
		
		List<List<String>> crit = criteria.asLists();
		for(List<String> row : crit) {
			if(row.get(0).equalsIgnoreCase("count")) {
				int cntval;
				char operation = '=';
				boolean failed = false;
				try {	cntval = Integer.parseInt(row.get(1)); } 
				catch(Exception e) {	cntval = Integer.parseInt( row.get(1).substring(1));
					operation = row.get(1).charAt(0);
				}
				switch(operation) {
				case '=':
					if(booking.length != cntval) failed = true;
				case '>':
					if(booking.length <= cntval) failed = true;
				}
				if(failed) {
					log.log(Level.SEVERE, "Criteria '" + row.get(0) + "' failed.");
					throw new Exception("Criteria '" + row.get(0) + "' failed.");
				}
			}else if(row.get(0).contains(":")) {
				int objPos = Integer.parseInt(row.get(0).split(":")[0]);
				String objField = row.get(0).split(":")[1];
				if(!booking[objPos].validate(objField, row.get(1))) {
					log.log(Level.SEVERE, "Criteria '" + row.get(0) + "' failed.");
					throw new Exception("Criteria '" + row.get(0) + "' failed.");
				}
			}else {
				String err = "Invalid Criteria format defined, please check the documentation.";
				log.log(Level.SEVERE, err);
				throw new Exception(err);
			}
		}
		log.log(Level.INFO, "Exiting APICall.validateJsonWithDataTable");
	}
	
	@Given("^User calls Rest API \"(.*?)\" with \"(.*?)\" method with Create Booking data and save response as \"(.*?)\"$")
	public void callAPIWithDataSaveResponse(String URL, String method, String filename, DataTable data) throws Exception {
		log.log(Level.INFO, "Entering APICall.callAPIWithDataSaveResponse");
		Response resp = null;
		
		URL = Tokenizer.getTokenizer(URL).getValue();
		filename = Tokenizer.getTokenizer(filename).getValue();
		CreateBooking booking = new CreateBooking(data);
		RequestSpecification req = RestAssured.given();
		req.header("Content-Type", "application/json");
		req.body(booking);
		
		switch(method.toLowerCase()) {
		case "post":
			resp = req.post(URL);
			String t = resp.getCookie("__cfduid");
			FileHandlr.writePropConfig(cacheFile, new String[][] {{"__cfduid",t}});
			break;
		case "put":
			String cook = Tokenizer.getTokenizer("'cache:__cfduid'").getValue();
			Map<String, String> cookie = new HashMap<String, String>();
			cookie.put("__cfduid", cook);
			cookie.put("token", cook);
			req.cookies(cookie);
			resp = req.put(URL);
		}

		
		FileHandlr.writeFile(true, CacheFolder, filename, resp.asString());
		
		log.log(Level.INFO, "Exiting APICall.callAPIWithDataSaveResponse");
	}
	
	@Given("^User saves booking id from CreatBookingResponse Json \"(.*?)\" as \"(.*?)\"$")
	public void saveValueFromJson(String jsonFile, String key) throws Exception{
		jsonFile = Tokenizer.getTokenizer(jsonFile).getValue();
		key = Tokenizer.getTokenizer(key).getValue();
		
		Gson jr = new Gson();
		CreateBookingResponse obj = jr.fromJson(jsonFile, CreateBookingResponse.class);
		FileHandlr.writePropConfig(cacheFile, new String[][] {{key, String.valueOf(obj.bookingid)}});
	}
	
	@Given("^User validates Get Booking Json response \"(.*?)\" having criteria$")
	public void validateBookingJsonWithDataTable(String Json, DataTable criteria) throws Exception{
		log.log(Level.INFO, "Entering APICall.validateBookingJsonWithDataTable");
		Json = Tokenizer.getTokenizer(Json).getValue();
		
		Gson jr = new Gson();
		GetBooking booking = jr.fromJson(Json, GetBooking.class);
		
		List<List<String>> crit = criteria.asLists();
		for(List<String> row : crit) {
				if(!booking.validate(row.get(0), row.get(1))) {
					log.log(Level.SEVERE, "Criteria '" + row.get(0) + "' failed.");
					throw new Exception("Criteria '" + row.get(0) + "' failed.");
				}
		}
		log.log(Level.INFO, "Exiting APICall.validateBookingJsonWithDataTable");
	}
}
