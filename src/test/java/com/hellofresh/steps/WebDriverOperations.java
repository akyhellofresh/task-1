package com.hellofresh.steps;

import static com.hellofresh.runner.FrameworkStart.cacheFile;
import static com.hellofresh.runner.FrameworkStart.driver;
import static com.hellofresh.runner.FrameworkStart.log;
import static com.hellofresh.runner.FrameworkStart.wait;

import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.hellofresh.commonlibrary.FileHandlr;
import com.hellofresh.commonlibrary.Tokenizer;

import cucumber.api.java.en.Given;

public class WebDriverOperations {

	@Given("^User navigates to \"(.*?)\"$")
	public void navigation(String url) throws Exception{
		log.log(Level.INFO, "Entering WebDriverOperations.navigation");
		url = Tokenizer.getTokenizer(url).getValue();
		driver.navigate().to(url);
		log.log(Level.INFO, "Exiting WebDriverOperations.navigation");
	}
	
	@Given("^User switch to frame \"(.*?)\"$")
	public void switchFrame(String control) throws Exception{
		log.log(Level.INFO, "Entering WebDriverOperations.switchFrame");
		if(control.equalsIgnoreCase("default")) {
			driver.switchTo().defaultContent();
		}else {
			WebElement ele = getElementWhenVisible(control);
			driver.switchTo().frame(ele);
		}
		log.log(Level.INFO, "Exiting WebDriverOperations.switchFrame");
	}
	
	@Given("^User clicks on \"(.*?)\"$")
	public void click(String data) throws Exception{
		log.log(Level.INFO, "Entering WebDriverOperations.click");
		getElementWhenVisible(data).click();
		log.log(Level.INFO, "Exiting WebDriverOperations.click");
	}
	
	@Given("^User enters text \"(.*?)\" in \"(.*?)\"$")
	public void enterText(String data, String control) throws Exception{
		log.log(Level.INFO, "Entering WebDriverOperations.enterText");
		getElementWhenVisible(control).sendKeys(Tokenizer.getTokenizer(data).getValue());
		log.log(Level.INFO, "Exiting WebDriverOperations.enterText");
	}
	
	@Given("^User selects \"(Value|Text|Index)\" = \"(.*?)\" from dropdown \"(.*?)\"$")
	public void selectDropDown(String selectionCriteria, String value, String control) throws Exception{
		log.log(Level.INFO, "Entering WebDriverOperations.selectDropDown");
		value = Tokenizer.getTokenizer(value).getValue();
		Select select = new Select(getElementWhenVisible(control));
		
		if(selectionCriteria.equalsIgnoreCase("Value")) {
			select.selectByValue(value);
		}else if(selectionCriteria.equalsIgnoreCase("Text")) {
			select.selectByVisibleText(value);
		}else if(selectionCriteria.equalsIgnoreCase("Index")) {
			select.selectByIndex(Integer.parseInt(value));
		}
		log.log(Level.INFO, "Exiting WebDriverOperations.selectDropDown");
	}
	
	@Given("^User fetch property \"(.*?)\" of Web-Control \"(.*?)\" and save it as \"(.*?)\"$")
	public void getAndSaveProperty(String prop, String control, String key) throws Exception{
		log.log(Level.INFO, "Entering WebDriverOperations.getAndSaveProperty");
		String result = "";
		WebElement ele = getElementWhenVisible(control);
		prop = Tokenizer.getTokenizer(prop).getValue();
		key = Tokenizer.getTokenizer(key).getValue();
		switch(prop.toLowerCase()) {
		case "displayed":
		case "isdisplayed":
			result = String.valueOf(ele.isDisplayed());
			break;
		case "text":
			result = ele.getText();
			break;
		}
		FileHandlr.writePropConfig(cacheFile, new String[][] {{key, result}});
		log.log(Level.INFO, "Exiting WebDriverOperations.getAndSaveProperty");
	}
	
	@Given("^User save current URL as \"(.*?)\"$")
	public void saveCurrentURL(String key) throws Exception{
		log.log(Level.INFO, "Entering WebDriverOperations.saveCurrentURL");
		FileHandlr.writePropConfig(cacheFile, new String [][] {{key, driver.getCurrentUrl()}});
		log.log(Level.INFO, "Exiting WebDriverOperations.saveCurrentURL");
	}
	
	@Given("^User sleeps for \"(.*?)\" seconds$")
	public void userSleep(String seconds) throws Exception{
		log.log(Level.INFO, "Entering WebDriverOperations.saveCurrentURL");
		long millis = Integer.parseInt(seconds) * 1000; 
		Thread.sleep(millis);
		log.log(Level.INFO, "Exiting WebDriverOperations.saveCurrentURL");
	}
	
	//Private Common Functions
	public WebElement getElementWhenVisible(String token) throws Exception{
		log.log(Level.INFO, "Entering WebDriverOperations.getElementWhenVisible with token : '" + token  + "'");
		WebElement result = null;
		By crit = Tokenizer.getTokenizer(token).getObjectRepoInstance().getSearchCriteria();
		try { result = driver.findElement(crit); }
		catch(NoSuchElementException nsee) {
			try { result = wait.until(ExpectedConditions.visibilityOfElementLocated(crit)); }
			catch(Exception e) {
				String err = "Could not find webelement from token : '" + token + "'";
				log.log(Level.SEVERE, err);
				throw new Exception(err, e);
			}
		}
		log.log(Level.INFO, "Exiting WebDriverOperations.getElementWhenVisible with found WebElement");
		return result;
	}
}
