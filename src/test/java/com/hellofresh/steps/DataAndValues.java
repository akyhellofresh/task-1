package com.hellofresh.steps;

import static com.hellofresh.runner.FrameworkStart.cacheFile;
import static com.hellofresh.runner.FrameworkStart.log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;

import com.hellofresh.commonlibrary.FileHandlr;
import com.hellofresh.commonlibrary.Tokenizer;

import cucumber.api.java.en.Given;

public class DataAndValues {
	
	@Given("^User generates data for action \"(.*?)\" with value \"(.*?)\" and save to \"(.*?)\"$")
	public boolean stringGeneration(String action,String format,String key) throws Exception{
        boolean flag=false;
        try{
        	log.log(Level.INFO,"Entring DataAndValues.stringGenerating using operation"+action+" with value "+format);
        	action = Tokenizer.getTokenizer(action).getValue();
        	format = Tokenizer.getTokenizer(format).getValue();
        	key = Tokenizer.getTokenizer(key).getValue();
        	
            if(action.equalsIgnoreCase("getDate")){
                String timeStamp = new SimpleDateFormat(format).format(new Date());
                FileHandlr.writePropConfig(cacheFile, new String[][] {{key, timeStamp.trim()}});
            }else if(action.equalsIgnoreCase("getDateTime")){
            	String timeStamp = String.valueOf(new Date().getTime());
            	FileHandlr.writePropConfig(cacheFile, new String[][] {{key, timeStamp.trim()}});
            }
            else if(action.equalsIgnoreCase("UniqueString")){
            	String prefix = null;
            	String result = null;
            	int count = 0;
            	if(format.contains(":")) {
            			prefix = format.split(":")[0];
            			count = Integer.parseInt(format.split(":")[1]);
            			result = prefix + RandomStringUtils.randomNumeric(count);
            	}
            	else {
            		count = Integer.parseInt(format);
            		result = RandomStringUtils.randomNumeric(count);
            	}
            	FileHandlr.writePropConfig(cacheFile, new String[][] {{key, result}});
            }else if(action.equalsIgnoreCase("RandomAlphaNumeric")){
            	String s = RandomStringUtils.randomAlphanumeric(Integer.parseInt(format));
            	FileHandlr.writePropConfig(cacheFile, new String[][] {{key, s.trim()}});
            }else if(action.equalsIgnoreCase("RandomNumeric")){
            	String s = RandomStringUtils.randomNumeric(Integer.parseInt(format));
            	if(s.charAt(0)=='0')	s = s.substring(1);
            	FileHandlr.writePropConfig(cacheFile, new String[][] {{key, s.trim()}});
            }else if(action.equalsIgnoreCase("RandomAlphabetic")){
            	String s = RandomStringUtils.randomAlphabetic(Integer.parseInt(format));
            	FileHandlr.writePropConfig(cacheFile, new String[][] {{key, s.trim()}});
            }else if(action.equalsIgnoreCase("dateadddays")){
            	Calendar c = Calendar.getInstance();
            	c.add(Calendar.DAY_OF_MONTH, Integer.parseInt(format));
            	String s = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
            	FileHandlr.writePropConfig(cacheFile, new String[][] {{key, s.trim()}});
            }else{
                String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
                timeStamp=timeStamp.replaceAll("\\.","");
                FileHandlr.writePropConfig(cacheFile, new String[][] {{key, format.trim() + timeStamp.trim()}});
            }
           flag=true;
        }catch (Exception e){
        	log.log(Level.SEVERE, e.getMessage());
        	throw e;
        }
        log.log(Level.INFO, "Exiting DataAndValues.stringGeneration");
        return flag;
    }

	@Given("^User perform operation \"(.*?)\" on \"(.*?)\" and \"(.*?)\" then save results in \"(.*?)\"$")
	public void performOperation(String operation, String val1, String val2, String Key) throws Exception{
		 log.log(Level.INFO, "Entering DataAndValues.performOperation " + operation + " with value " + val1 + " and " + val2);
		 val1 = Tokenizer.getTokenizer(val1).getValue();
		 val2 = Tokenizer.getTokenizer(val2).getValue();
		 Key = Tokenizer.getTokenizer(Key).getValue();
		 String strKey = "";
         switch (operation){
             case "prefixstring":
                strKey = val1.substring(0,val1.indexOf(val2));
                FileHandlr.writePropConfig(cacheFile, new String[][] {{Key, strKey}});  
                break;
             case "postfixstring":
                 strKey = val1.substring(val1.indexOf(val2)+1);
                 FileHandlr.writePropConfig(cacheFile, new String[][] {{Key, strKey}});
                 break;
             case "stringConcat":
            	 strKey = val1.concat(val2);
            	 FileHandlr.writePropConfig(cacheFile, new String[][] {{Key, strKey}});
            	 break;
             default:
            	 log.log(Level.SEVERE, "Operation '" + operation + "' not found");
            	 break;
         }
         log.log(Level.INFO, "Exiting DataAndValues.performOperation");
	}
	
	@Given("^User verifies \"(.*?)\" is \"(true|false)\"$")
	public void verifyTrueFalse(String data, String op) throws Exception{
		log.log(Level.INFO, "Entering DataAndValues.verifyTrueFalse");
		data = Tokenizer.getTokenizer(data).getValue();
		if(Boolean.valueOf(op)) {
			Assert.assertTrue(Boolean.valueOf(data));
		}else {
			Assert.assertFalse(Boolean.valueOf(data));
		}
		log.log(Level.INFO, "Exiting DataAndValues.verifyTrueFalse");
	}
	
	@Given("^User verifies \"(.*?)\" \"(equal|contain)\" \"(.*?)\"$")
	public void verifyEqualsOrContains(String data1, String operation, String data2) throws Exception{
		log.log(Level.INFO, "Entering DataAndValues.verifyEqualsOrContains");
		data1 = Tokenizer.getTokenizer(data1).getValue();
		data2 = Tokenizer.getTokenizer(data2).getValue();
		switch(operation.toLowerCase()) {
		case "equal":
		case "equals":
			Assert.assertEquals(data1, data2);
			break;
		case "contain":
		case "contains":
			if(!data1.contains(data2)) {
				throw new Exception("'" + data1 + "' does not contain '" + data2 + "'");
			}
			break;
		}
		log.log(Level.INFO, "Exiting WebDriverOperations.verifyEqualsOrContains");
	}
}
