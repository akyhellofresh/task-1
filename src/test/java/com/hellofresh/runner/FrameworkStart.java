package com.hellofresh.runner;

import java.io.File;
import java.net.InetAddress;
import java.util.TreeMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hellofresh.commonlibrary.FileHandlr;
import com.hellofresh.commonlibrary.WebDriverFactory;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/resources/TestCases/"}
		,glue={"com.hellofresh.steps", "com.hellofresh.runner"}
		,plugin={"pretty", "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "html:reports"}
		)
public class FrameworkStart {
	
	// Configuration Variables
	public static String ResourcesFolder = "src/test/resources/";
	public static String CacheFolder = "src/test/resources/Cache/";
	public static String configPath = "src/test/resources/config.yml";
	public static String cacheFile, ORPath, LogFilePath, hubIP, GridURL;
	public static String screenshotsPath, ReportPath;
	public static boolean parallelExecution, AutoConfigureGrid;
	public static int nodes;
	
	//Instance Variables
	public static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	public static TreeMap<Integer, Process> process = new TreeMap<Integer, Process>();
	public static RemoteWebDriver driver = null;
	public static WebDriverWait wait = null;
	
	@BeforeClass
	public static void setup() throws Exception{
		log.log(Level.INFO, "Entering FrameworkStart.setup");
		
		try { LogFilePath = FileHandlr.getYamlValue(configPath, "LogFile"); log.addHandler(new FileHandler(LogFilePath)); }catch(Exception e) {	log.log(Level.SEVERE, "Unable to Register file logs, moving ahead!!!");	}
		log.log(Level.INFO, "Log File setup complete");
		
		try { ORPath = FileHandlr.getYamlValue(configPath, "OR"); }catch(Exception e) {	log.log(Level.SEVERE, "Unable to find appropriate Object Repository, cannot proceed. Exiting!!!");	System.exit(-1);	}
		log.log(Level.INFO, "Object Repository setup complete");
		
		try { 
			screenshotsPath = FileHandlr.getYamlValue(configPath, "Screenshots");
			ReportPath = FileHandlr.getYamlValue(configPath, "ReportPath");
		} catch(Exception e) { log.log(Level.SEVERE, "Incorrect path for Screenshots!!!");	}
		
		FileHandlr.folderCleanup(ReportPath);
		FileHandlr.folderCleanup(screenshotsPath);
		
		try { 
			cacheFile = FileHandlr.getYamlValue(configPath, "CacheFile");
			File f = new File(cacheFile);
			if(f.exists())	f.delete();
			f.createNewFile();
		}
		catch(Exception e) { 
			cacheFile = CacheFolder + "Cache_" + System.currentTimeMillis() / 1000 + ".cache"; 
			log.log(Level.SEVERE, "Unable to find defined cache, creating another : '" + cacheFile + "'"); 
		}
		log.log(Level.INFO, "Cache File setup complete");
		
		try {
			parallelExecution = Boolean.parseBoolean(FileHandlr.getYamlValue(configPath, "parallelExecution"));
			AutoConfigureGrid = Boolean.parseBoolean(FileHandlr.getYamlValue(configPath, "AutoConfigureGrid"));
			nodes = Integer.parseInt(FileHandlr.getYamlValue(configPath, "Nodes"));
			if(AutoConfigureGrid)	ConfigureGrid(nodes);
			int waitTimeout = Integer.parseInt(FileHandlr.getYamlValue(configPath, "driverWaitTimeout"));
			int sleep = Integer.parseInt(FileHandlr.getYamlValue(configPath, "driverSleep"));
			driver = WebDriverFactory.Initialize(configPath);
			wait = new WebDriverWait(driver, waitTimeout, sleep);
		}catch(Exception e) {
			log.log(Level.SEVERE, "Custom WebDriverWait Configuration failed. Now kept default values - WaitTimeOut(10), Sleep(50).");
			wait = new WebDriverWait(driver, 10, 50);
		}
		log.log(Level.INFO, "WebDriverWait values setup complete");
		log.log(Level.INFO, "Exiting FrameworkStart.setup");
	}
	
	@AfterClass
	public static void cleanup() throws Exception{
		log.log(Level.INFO, "Entering FrameworkStart.cleanup");
		
		for(Process p : process.values()){
			p.destroyForcibly();
		}
		
		FileHandlr.fileDelete(ResourcesFolder + "hub.bat");
		FileHandlr.fileDelete(ResourcesFolder + "node.bat");
		
		if(driver != null) {
			driver.quit();
			driver = null;
		}
		log.log(Level.INFO, "Exiting FrameworkStart.cleanup");
	}
	
	//private methods
	private static void ConfigureGrid(int nodes) throws Exception{
		log.log(Level.INFO, "Entering FrameworkStart.ConfigureGrid with '" + nodes + "' nodes");
		
		hubIP = InetAddress.getLocalHost().getHostAddress().trim();
		GridURL = "http://" + hubIP + ":4444/wd/hub";
		
		//Configure Hub
		String hubData = "@echo off\r\n" + "set \"curpath=%cd%\"\r\n" +	"java -jar selenium-server-standalone.jar -role hub\r\n" +	"pause";
		FileHandlr.writeFile(true, ResourcesFolder, "hub.bat", hubData);
		HubRunner();
		Thread.sleep(2000);
		
		//Configure Node
		String nodeData = "@echo off\r\n" + 
					"java -Dwebdriver.chrome.driver=%~dp0\\chromedriver.exe -jar %~dp0\\selenium-server-standalone.jar -role node -hub http://"+
					hubIP + ":4444/grid/register, -browser browserName=chrome,maxInstances=3,maxSession=2 %1 %2\r\n" + 
					"pause";
		FileHandlr.writeFile(true, ResourcesFolder, "node.bat", nodeData);
		int runs= 1;
		while(runs <= nodes) {
			NodeRunner();
			runs++;
		}
		Thread.sleep(1000);
		log.log(Level.INFO, "Exiting FrameworkStart.ConfigureGrid");
	}
	
	public static void HubRunner(){
		try {
			FrameworkStart.log.log(Level.INFO, "Trying Running Hub");
			File dir = new File(FrameworkStart.ResourcesFolder);
			FrameworkStart.process.put(0, Runtime.getRuntime().exec("cmd /c start \"\" hub.bat", null, dir));
		}catch(Exception e) {
			FrameworkStart.log.log(Level.SEVERE, "Could not run Hub. Grid setup failed, execution will be done sequentially");
		}
	}
	
	public static void NodeRunner() {
		try {
			log.log(Level.INFO, "Trying to run Node.");
			File dir = new File(FrameworkStart.ResourcesFolder);
			int lk = process.lastKey() + 1;
			process.put(lk,Runtime.getRuntime().exec("cmd /c start \"\" node.bat -port " + (5555 + lk), null, dir));
			
		}catch(Exception e) {
			log.log(Level.SEVERE, "Could not run Node. Grid setup failed, execution will be done sequentially");
			return;
		}
	}
}