package com.hellofresh.runner;

import static com.hellofresh.runner.FrameworkStart.*;

import java.io.File;
import java.util.logging.Level;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.google.common.io.Files;
import com.hellofresh.commonlibrary.WebDriverFactory;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


public class Pre_requisite {
	int screenshotCounter = 0;
	
	@Before
	public void ScenarioSpecificSetup() {
    	log.log(Level.INFO, "Entering 'Pre_requisite.ScenarioSpecificSetup'");
    	if(driver==null)	driver = WebDriverFactory.Initialize(configPath);
    	log.log(Level.INFO, "Exiting 'Pre_requisite.ScenarioSpecificSetup'");
	}
	
	@After
	public void ScenarioCleanup(Scenario scenario) {
    	log.log(Level.INFO, "Entering 'Pre_requisite.ScenarioCleanup'");
    	if(scenario.isFailed()) {
    		File ss = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    		byte[] byt = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    		try { Files.copy(ss, new File(screenshotsPath + "ss_" + screenshotCounter + ".png")); }catch(Exception e) {log.log(Level.SEVERE, "Unable to process screenshot");}
    		scenario.embed(byt, "image/png");
    	}
		driver.quit();
		driver = null;
    	log.log(Level.INFO, "Exiting 'Pre_requisite.ScenarioCleanup'");
	}
}
