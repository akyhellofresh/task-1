package com.hellofresh.commonlibrary;

import static com.hellofresh.runner.FrameworkStart.log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;

import org.yaml.snakeyaml.Yaml;

public class FileHandlr {
	public static String getYamlValue(String filePath, String yamlToken) throws Exception {
        log.log(Level.INFO, "Entering 'FileHandlr.getYamlValue', searching : '" + yamlToken + "' in file : '" + filePath + "'");
        int tokenCount = 0, i = 0;
        Map<?, ?> map = null;
        String val = null;
        StringTokenizer st = new java.util.StringTokenizer(yamlToken, ".");
        try {
            Reader reader = new FileReader(filePath);
            Yaml yaml = new Yaml();
            map = (Map<?, ?>) yaml.load(reader);
            tokenCount = st.countTokens();
            for (i = 1; i < tokenCount; i++) {
                String token = st.nextToken();
                map = (Map<?, ?>) map.get(token);
            }
            val = map.get(st.nextToken()).toString();
			reader.close();
			log.log(Level.INFO, "Exiting 'FileHandlr.getYamlValue' with value : '" + val + "'");
            return val;
        } catch (Exception e) {
        	String err = "Exiting 'FileHandlr.getYamlValue' with exception, while trying searching : '" + yamlToken + "' in file : '" + filePath + "'";
        	log.log(Level.SEVERE, err, e);
			throw new Exception(err, e);
        }
    }
	
	public static String[][] ReadYaml(String filePath, String yamlToken) throws Exception {
		log.log(Level.INFO, "Entering FileHandlr.ReadYaml with file path : '" + filePath + "', Yaml Token : '" + yamlToken + "'");
        String[][] val = new String[][]{	{yamlToken, ""}	        };
        int tokenCount = 0, i = 0;
        Map<?, ?> map = null;

        StringTokenizer st = new java.util.StringTokenizer(yamlToken, ".");
        try {
            Reader reader = new FileReader(filePath);
            
            Yaml yaml = new Yaml();
            map = (Map<?, ?>) yaml.load(reader);
            tokenCount = st.countTokens();
            for (i = 1; i < tokenCount; i++) {
                String token = st.nextToken();
                map = (Map<?, ?>) map.get(token);
            }
            val[0][1] = map.get(st.nextToken()).toString();
			reader.close();
			log.log(Level.INFO, "Exiting FileHandlr.ReadYaml.");
            return val;
        } catch (Exception e) {
        	String err = "Error reading " + yamlToken + " from Object Repository. Please check yml file at : " + filePath;
        	log.log(Level.SEVERE, err);
			throw new Exception(err);
        }
    }
	
	public static String[][] ReadPropFile(String fileName, String... configs) throws Exception{
		log.log(Level.INFO, "Entering FileHandlr.ReadPropFile.");
		String config[][];
		Properties prop = new Properties();
		prop.load(new FileReader(fileName));
		if(configs.length > 0 && !(configs[0].isEmpty())){
			config = new String[configs.length][2];
		for(int i = 0;i<configs.length; i++){
				config[i][0] = configs[i];
				config[i][1] = prop.getProperty(configs[i]);
			}
		}
		else{
			config = new String[prop.stringPropertyNames().size()][2];
			Iterator<String> names = prop.stringPropertyNames().iterator();
			int i=0;
			while(names.hasNext()){
				config[i][0] = names.next();
				config[i][1] = prop.getProperty(config[i][0]);
				i++;
			}
		}
		log.log(Level.INFO, "Exiting FileHandlr.ReadPropFile.");
		return config;
	}
	public static boolean writePropConfig(String filePath, String[][] configs) throws Exception {
		log.log(Level.INFO, "Entering FileHandlr.WritePropConfig.");
		boolean result = false;
		Properties prop = new Properties();
		prop.load(new FileReader(filePath));
		//File f = new File(filePath);
		FileOutputStream OutStream = new FileOutputStream(filePath);
		if(configs.length > 0){
			try {
				for(int i = 0;i<configs.length; i++){
					prop.put(configs[i][0], configs[i][1]);
					//prop.setProperty(configs[i][0], configs[i][1]);
					result = true;
				}
			}catch(Exception e) {
				OutStream.close(); OutStream = null;
				throw new Exception("Invalid formatted values given for writing to config.");
			}
			prop.store(OutStream,"Automation Comments");
			OutStream.close(); OutStream = null;
		}
		else{
			OutStream.close(); OutStream = null;
			throw new Exception("No Data provided to write.");
		}
		log.log(Level.INFO, "Exiting FileHandlr.WritePropConfig.");
		return result;
	}
	
	public static void folderCleanup(String path) {
		log.log(Level.INFO, "Entering FileHandlr.folderCleanup for : '" + path + "'");
		File f = new File(path);
		if (f.isDirectory()) {
			File[] children = f.listFiles();
			for(File child : children) {
				if(child.isFile()) child.delete();
			}
		}
		log.log(Level.INFO, "Exiting FileHandlr.folderCleanup for : '" + path + "'");
	}
	
	public static void fileDelete(String filepath) throws Exception{
		log.log(Level.INFO, "Entering FileHandlr.filedelete for : '" + filepath + "'");
		File f = new File(filepath);
		if (f.exists()) {
			log.log(Level.SEVERE, "'" + filepath + "' does not exist, hence no deletion required."); 
		}else {
			if(f.isDirectory()) {
				String err = "'" + filepath + "' is a directory, Use folder cleanup method for directory cleanup.";
				log.log(Level.SEVERE, err);
				throw new Exception(err);
			}else {
				f.delete();
			}
		}
		log.log(Level.INFO, "Exiting FileHandlr.filedelete for : '" + filepath + "'");
	}
	
	public static String writeFile(boolean Overwrite, String DirPath, String FileName,String Data) throws Exception{
		String results = "";
		FileOutputStream myOutputStream;
		File myFile = null, myDir=null;
		try {
			myDir = new File(DirPath);
			myDir.mkdirs();
			FileName = DirPath + FileName;
			myFile = new File(FileName);
		   	if(Overwrite)
		   		myOutputStream = new FileOutputStream(myFile,false);
		   	else
		   		myOutputStream = new FileOutputStream(myFile,true);
		   	
		    	myOutputStream.write(Data.getBytes());
		    	myOutputStream.write(System.lineSeparator().getBytes());
		        myOutputStream.flush();
		        myOutputStream.close();
		} catch (Exception e) { throw e; }
		results = "Success";
		return results;
	}
	
	public static String readFile(String DirPath, String FileName) throws Exception{
		
		String results = "";
		BufferedReader myReader;
		File myFile = new File(DirPath + FileName);
		try{		
			myReader =  new BufferedReader(new FileReader(myFile));
			String newLine;
			while((newLine = myReader.readLine()) != null){	
				results = results + newLine + System.lineSeparator();
			}
			myReader.close();
		}catch(Exception e){	throw e;	}
		return results;
	}
}
