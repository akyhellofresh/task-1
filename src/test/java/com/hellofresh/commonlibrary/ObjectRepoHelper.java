package com.hellofresh.commonlibrary;

import static com.hellofresh.runner.FrameworkStart.log;

import java.util.Map;
import java.util.logging.Level;

import org.openqa.selenium.By;

import com.hellofresh.commonlibrary.Enums.WebLocatorType;

import io.cucumber.datatable.DataTable;

public class ObjectRepoHelper {
	By searchCriteria;
	WebLocatorType LocatorType;
	String fetchedValue;
	
	private ObjectRepoHelper(){}
	
	public static ObjectRepoHelper ReadOR(String filePath, String ORString, DataTable data) throws Exception{
		log.log(Level.INFO, "Entering ObjectRepoHelper.ReadOR with filePath : '" + filePath + "', OR Search query : '" + ORString + "'");
		if(ORString==null) {
			String err = "Null criteria is not supported, instead a blank criteria can be sent.";
			log.log(Level.SEVERE, err);
			throw new Exception(err);
		}
		String result = FileHandlr.ReadYaml(filePath, ORString)[0][1];
		ObjectRepoHelper orh = new ObjectRepoHelper();
		orh.ReadFetchedString(result, data);
		orh.searchCriteria = createSearchCriteria(orh.LocatorType, orh.fetchedValue);
		return orh;
	}
	
	private static By createSearchCriteria(WebLocatorType locator, String criteria) {
		By result = null;
		switch(locator) {
		case classname:
			result = By.className(criteria);
			break;
		case cssselector:
			result = By.cssSelector(criteria);
			break;
		case id:
			result = By.id(criteria);
			break;
		case linktext:
			result = By.linkText(criteria);
			break;
		case name:
			result = By.name(criteria);
			break;
		case partiallinktext:
			result = By.partialLinkText(criteria);
			break;
		case tagname:
			result = By.tagName(criteria);
			break;
		case xpath:
			result = By.xpath(criteria);
			break;
		}
		return result;
	}
	
	private void ReadFetchedString(String ORString, DataTable data) throws Exception{
		String value = null;
		if(ORString.contains("??")) {
			String[] splitted = ORString.split("\\?\\?");
			String locatorType = splitted[0];
			value = splitted[1];
			LocatorType = Enums.WebLocatorTypeIdentifier(locatorType);
		}else {
			throw new Exception("Not a Object Repository string as its missing '??' sign.");
		}
		
		if(data==null || data.isEmpty()) {
			fetchedValue = value;
		}else {
			fetchedValue = createORStringFromParameters(value, data);
		}
	}
	
	private String createORStringFromParameters(String RawString, DataTable data) throws Exception{
        Map<String, String> eventHashMap;
        eventHashMap=data.asMap(String.class, String.class);
        String Element = "";
        for(int i =0;i<eventHashMap.size();i++)
        {
            Object[] keySet=eventHashMap.keySet().toArray();
            String key=(String) keySet[i];
            String value=eventHashMap.get( (eventHashMap.keySet().toArray())[i] );
            value = Tokenizer.getTokenizer(value,null).getValue();
            Element = Element.concat(value);
            Element = Element.concat(",");
            RawString = RawString.replace(key,value);
        }
        return RawString;
	}
	
	//Getters
	public WebLocatorType getLocatorType() { return LocatorType;}
	public String getValue() { return fetchedValue; }
	public By getSearchCriteria() { return searchCriteria; }
}
