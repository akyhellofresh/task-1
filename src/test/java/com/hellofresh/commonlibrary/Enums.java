package com.hellofresh.commonlibrary;

public class Enums {

		public static enum Browser{chrome, ie, firefox, safari, headless};
		public static enum WebLocatorType{classname, cssselector, id, linktext, name, tagname, xpath, partiallinktext};
		public static enum TokenType{or, config, cache, cachefile, other, _blank_};
		
		public static Browser Browser_Identifier(String browser) throws Exception{
			return converter(Browser.class, browser);
		}
		
		public static WebLocatorType WebLocatorTypeIdentifier(String locatorType) throws Exception{
			return converter(WebLocatorType.class, locatorType);
		}
		
		public static TokenType TokenTypeIdentifier(String TokenType) throws Exception{
			return converter(TokenType.class, TokenType);
		}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public static <T extends Enum> T converter(Class<T> type, String value) throws Exception{
			try {
				return (T) Enum.valueOf(type, value.toLowerCase());
			}catch(IllegalArgumentException iae) {
					throw new IllegalArgumentException("Invalid value provided. Please check the input value.", iae);
			}catch(NullPointerException ex) {
					throw new NullPointerException("Null value is not accepted. Please check the input value.");
			}catch(Exception e) {
					throw e;
			}
		}
	
}
