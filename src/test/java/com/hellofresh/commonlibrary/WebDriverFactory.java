package com.hellofresh.commonlibrary;

import static com.hellofresh.runner.FrameworkStart.*;

import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.hellofresh.commonlibrary.Enums.Browser;

public class WebDriverFactory {
	
	private WebDriverFactory() {}
	
	public static RemoteWebDriver Initialize(String configPath) {
		log.log(Level.INFO, "Entering 'WebDriver.Initialize' with configPath : '" + configPath + "'");
		RemoteWebDriver driver = null;
		Browser browserType = null;
		try {
			String brow = FileHandlr.getYamlValue(configPath, "browser");
			browserType = Enums.Browser_Identifier(brow);
		}catch(Exception e){
			log.log(Level.SEVERE, "Unable to Identify a valid 'browser' from : " + configPath, e);
			System.exit(-1);
		}
		
		try {
			
			switch(browserType) {
			case chrome:
		        System.setProperty("webdriver.chrome.driver", FileHandlr.getYamlValue(configPath, "chromeDriverPath"));
		        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setBrowserName("chrome");
				capabilities.setPlatform(org.openqa.selenium.Platform.ANY);		        
				ChromeOptions co = new ChromeOptions();
				co.addArguments("test-type");
				co.addArguments("--disable-extensions");
				co.addArguments("--start-maximized");
				capabilities.setCapability(ChromeOptions.CAPABILITY, co);
				if(parallelExecution) {
					driver = new RemoteWebDriver(new URL(GridURL), capabilities);
				}else {
					driver = new ChromeDriver(co);
				}
				break;
			case firefox:
				//TODO implement this section
				break;
			case ie:
				//TODO implement this section
				break;
			case safari:
				//TODO implement this section
				break;
			case headless:
				//TODO implement this section
				break;
			}
		}catch(Exception e) {
			log.log(Level.SEVERE, "Unable to Initialize driver of type : '" + browserType + "'. Check the config file for correct path value.");
			System.exit(-1);
		}
		int implicitWait = 0;
		try {	implicitWait = Integer.parseInt(FileHandlr.getYamlValue(configPath, "driverImplicitWait"));	}	catch(Exception r) {}
		if(implicitWait != 0)
			driver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
		log.log(Level.INFO, "Exiting 'WebDriver.Initialize' after successful driver initialization.");
		return driver;
	}
	
}
