package com.hellofresh.commonlibrary;

import static com.hellofresh.runner.FrameworkStart.*;

import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;

import com.hellofresh.commonlibrary.Enums.TokenType;

import io.cucumber.datatable.DataTable;


public class Tokenizer {
	private Tokenizer() {}
	
	ObjectRepoHelper objRepoInst;
	TokenType tokenType;
	String RawTokenType, RawValue, fetchedValue = "";
	
	//Public Methods
	public static Tokenizer getTokenizer(String value) throws Exception{
		return getTokenizer(value, null);
	}
	
	public static Tokenizer getTokenizer(String value, DataTable data) throws  Exception{
		log.log(Level.INFO, "Entering Tokenizer.getTokenizer with value : '" + value + "' and a DataTable.");
		Tokenizer myToken = new Tokenizer();
		if(value.contains("'") ) {
			int count = StringUtils.countMatches(value,"'");
			if(count % 2 == 0){
				String[] splittedTokens = value.split("'");
				for(String token : splittedTokens) {
					if(token.contains(":")) {
						myToken.RawTokenType = token.split(":")[0];
						myToken.RawValue = token.split(":")[1];
						myToken.tokenType = Enums.TokenTypeIdentifier(myToken.RawTokenType);
						switch(myToken.tokenType){
						case cache:
							myToken.fetchedValue = myToken.fetchedValue + FileHandlr.ReadPropFile(cacheFile, myToken.RawValue)[0][1];
							break;
						case cachefile:
							myToken.fetchedValue = myToken.fetchedValue + FileHandlr.readFile(CacheFolder, myToken.RawValue);
							break;
						case config:
							myToken.fetchedValue = myToken.fetchedValue + FileHandlr.getYamlValue(configPath, myToken.RawValue);
							break;
						case or:
							myToken.objRepoInst = ObjectRepoHelper.ReadOR(ORPath, myToken.RawValue, data);
							myToken.fetchedValue = myToken.fetchedValue + myToken.objRepoInst.fetchedValue;
							break;
						case _blank_:
						case other:
							String err = "Operation not suported: " + myToken.tokenType;
							log.log(Level.SEVERE, err);
							throw new Exception(err);
						}
					}else {
						myToken.fetchedValue = myToken.fetchedValue + token;
						myToken.tokenType = Enums.TokenType._blank_;
					}
				}
			}else {
				myToken.fetchedValue = value;
				myToken.tokenType = Enums.TokenType._blank_;
			}
		}else {
			myToken.fetchedValue = value;
			myToken.tokenType = Enums.TokenType._blank_;
		}
		return myToken;
	}
	
	//Getters
	public String getValue() 		{ return fetchedValue;	}
	public TokenType getTokenType() { return tokenType;		}
	public String getRawTokenType() { return RawTokenType;	}
	public ObjectRepoHelper getObjectRepoInstance()	{	return objRepoInst;	}	
}
