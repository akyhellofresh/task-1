Feature: HelloFresh Task-2

	Scenario:  Create and Get Bookings
# Creating Pre-requisite Data
		When User perform operation "stringConcat" on "'config:ApiBaseURL'" and "/" then save results in "URL"
		And User generates data for action "RandomNumeric" with value "5" and save to "prefixer"
		And User generates data for action "RandomNumeric" with value "2" and save to "bookingid"
		And User generates data for action "RandomNumeric" with value "5" and save to "roomid"
		And User generates data for action "getDate" with value "yyyy-MM-dd" and save to "checkin"
		And User generates data for action "dateadddays" with value "5" and save to "checkout"
    And User perform operation "stringConcat" on "mystyle'cache:prefixer'" and "@aky'cache:prefixer'.com" then save results in "email"
		And User generates data for action "UniqueString" with value "First:4" and save to "firstname"
		And User generates data for action "UniqueString" with value "Last:5" and save to "lastname"
		And User generates data for action "RandomNumeric" with value "12" and save to "phone"
# Create Booking
		And User calls Rest API "'cache:URL'" with "post" method with Create Booking data and save response as "createBooking.json"
		| checkin 		| 'cache:checkin'					 |
		| checkout 		| 'cache:checkout'				 |
		| bookingid   | 'cache:bookingid' 			 |
		| depositpaid | true 										 |
		| email				|	'cache:email'						 |
		| firstname   | 'cache:firstname'				 |
		| lastname   	| 'cache:lastname'				 |
		| phone				| 'cache:phone'						 |
		| roomid			| 'cache:roomid'	  			 |
# Get and verify all bookings count should be greater than 1
		And User calls Rest API "'cache:URL'" with "get" method and save response as "getBookings.json"
		Then User validates Get Bookings Json response "'cachefile:getbookings.json'" having criteria
		| count       |	>1											 |
# Get the created booking and verify all the data
		When User saves booking id from CreatBookingResponse Json "'cachefile:createBooking.json'" as "createdBookingId"
		And User perform operation "stringConcat" on "'cache:URL'" and "'cache:createdBookingId'" then save results in "GetBookingURL"
		And User calls Rest API "'cache:GetBookingURL'" with "get" method and save response as "getBooking.json"
		Then User validates Get Booking Json response "'cachefile:getbooking.json'" having criteria
		| checkin 		| 'cache:checkin'					 |
		| checkout 		| 'cache:checkout'				 |
		| bookingid   | 'cache:createdBookingId' |
		| depositpaid | true 										 |
		| firstname   | 'cache:firstname'				 |
		| lastname   	| 'cache:lastname'				 |
		| roomid			| 'cache:roomid'	  			 |
# Update Booking
		When User perform operation "stringConcat" on "'cache:firstname'" and "_updated" then save results in "updFName"
		And User perform operation "stringConcat" on "'cache:lastname'" and "_updated" then save results in "updLName"
		And User perform operation "stringConcat" on "'cache:roomid'" and "0" then save results in "updroom"
#		And User calls Rest API "'cache:GetBookingURL'" with "put" method with Create Booking data and save response as "updateBooking.json"
#		| checkin 		| 'cache:checkin'					 |
#		| checkout 		| 'cache:checkout'				 |
#		| bookingid   | 'cache:createdBookingId' |
#		| depositpaid | false										 |
#		| email				|	'cache:email'						 |
#		| firstname   | 'cache:updFName'				 |
#		| lastname   	| 'cache:updLName'				 |
#		| phone				| 'cache:phone'						 |
#		| roomid			| 'cache:updroom'	  			 |
		
		