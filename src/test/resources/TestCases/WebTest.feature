Feature: HelloFresh Task-1

  Scenario Outline: TestCase-1 Sign In
    When User navigates to "'config:AppBasicURL'"
# Home Page
    And User clicks on "'or:HomePage.SignIn'"
# Login Page
    Given User generates data for action "getDateTime" with value "" and save to "timestamp"
    And User perform operation "stringConcat" on "hf_challenge_'cache:timestamp'" and "@hf'cache:timestamp'.com" then save results in "email"
   	When User enters text "'cache:email'" in "'or:Login.SignUp_Email'"
   	And User clicks on "'or:Login.SignUp_Submit'"
# Sign-Up Page
		And User clicks on "'or:SignUp.Gender'"
		And User enters text "<fName>" in "'or:SignUp.FirstName'"
		And User enters text "<lName>" in "'or:SignUp.LastName'"
		And User enters text "<pwd>" in "'or:SignUp.Password'"
		And User selects "Value" = "<day>" from dropdown "'or:SignUp.DOB_Days'"
		And User selects "Value" = "<month>" from dropdown "'or:SignUp.DOB_Month'"
		And User selects "Value" = "<year>" from dropdown "'or:SignUp.DOB_Year'"
		And User enters text "<cmpny>" in "'or:SignUp.Company'"
		And User enters text "<add1>" in "'or:SignUp.Address_Line1'"
		And User enters text "<add2>" in "'or:SignUp.Address_Line2'"
		And User enters text "<city>" in "'or:SignUp.City'"
		And User selects "Text" = "<state>" from dropdown "'or:SignUp.State'"
		And User enters text "<postcode>" in "'or:SignUp.ZipCode'"
		And User enters text "<otherinfo>" in "'or:SignUp.AdditionalInfo'"
		And User enters text "<phone>" in "'or:SignUp.HomePhone'"
		And User enters text "<mobile>" in "'or:SignUp.MobilePhone'"
		And User enters text "<alias>" in "'or:SignUp.Alias'"
		And User clicks on "'or:SignUp.Submit'"
# User Account Page
		And User fetch property "Text" of Web-Control "'or:Account.Heading'" and save it as "heading"
		Then User verifies "'cache:heading'" "equal" "MY ACCOUNT"
		When User fetch property "Text" of Web-Control "'or:Account.Name'" and save it as "accountName"
		Then User verifies "'cache:accountName'" "equal" "<fName> <lName>"
		When User fetch property "Text" of Web-Control "'or:Account.WelcomeMessage'" and save it as "welcomeMessage"
		Then User verifies "'cache:welcomeMessage'" "contain" "Welcome to your account."
		When User fetch property "Displayed" of Web-Control "'or:Account.Logout'" and save it as "logoutDisplayed"
		Then User verifies "'cache:logoutDisplayed'" is "true"
		When User save current URL as "userAccountPageURL"
		Then User verifies "'cache:userAccountPageURL'" "contain" "controller=my-account"
		
   Examples:
   | fName     | lName    | pwd    | day | month | year | cmpny   | add1        | add2  | city   | state    | postcode | otherinfo | phone       | mobile      | alias |   
   | FirstName | LastName | Qwerty | 1   | 1     | 2000 | Company | Qwerty, 123 | zxcvb | Qwerty | Colorado | 12345    | Qwerty    | 12345123123 | 12345123123 | hf    |



	Scenario Outline: TestCase-2 Log In
    When User navigates to "'config:AppBasicURL'"
# Home Page
		And User clicks on "'or:HomePage.SignIn'"
# Login Page
		And User enters text "<email>" in "'or:Login.SignIn_Email'"
		And User enters text "<pwd>" in "'or:Login.SignIn_Pwd'"
		And User clicks on "'or:Login.SignIn_Submit'"
# User Account Page
		And User fetch property "Text" of Web-Control "'or:Account.Heading'" and save it as "heading"
		Then User verifies "'cache:heading'" "equal" "MY ACCOUNT"
		When User fetch property "Text" of Web-Control "'or:Account.Name'" and save it as "accountName"
		Then User verifies "'cache:accountName'" "equal" "<UserName>"
		When User fetch property "Text" of Web-Control "'or:Account.WelcomeMessage'" and save it as "welcomeMessage"
		Then User verifies "'cache:welcomeMessage'" "contain" "Welcome to your account."
		When User fetch property "Displayed" of Web-Control "'or:Account.Logout'" and save it as "logoutDisplayed"
		Then User verifies "'cache:logoutDisplayed'" is "true"
		When User save current URL as "userAccountPageURL"
		Then User verifies "'cache:userAccountPageURL'" "contain" "controller=my-account"
		
	Examples:
	| email 													 | pwd 			| UserName  |
	| hf_challenge_123456@hf123456.com | 12345678 | Joe Black |



	Scenario Outline: TestCase-3 Checkout
	  When User navigates to "'config:AppBasicURL'"
# Home Page
		And User clicks on "'or:HomePage.SignIn'"
# Login Page
		And User enters text "<email>" in "'or:Login.SignIn_Email'"
		And User enters text "<pwd>" in "'or:Login.SignIn_Pwd'"
		And User clicks on "'or:Login.SignIn_Submit'"
# Shopping
		And User clicks on "'or:Account.WomenSectionLink'"
		And User clicks on "'or:WomenSection.FadedShortSleeveTShirt'"
		And User sleeps for "5" seconds
		And User switch to frame "'or:WomenSection.ItemFrame'"
		And User clicks on "'or:WomenSection.FadedShortSleeveTShirt_bigpic'"
		And User switch to frame "default"
		And User clicks on "'or:FadedShortSleeveTShirt.AddToCart'"
		And User clicks on "'or:FadedShortSleeveTShirt.ProceedChckOut'"
		And User clicks on "'or:CartSummary.ProceedChckOut'"
		And User clicks on "'or:CartAddress.ProceedChckOut'"
		And User clicks on "'or:CartShipping.TermsCondition'"
		And User clicks on "'or:CartShipping.ProceedChckOut'"
		And User clicks on "'or:Payment.BankWire'"
		And User clicks on "'or:OrderSummary.Confirm'"
#Verification
		And User fetch property "Text" of Web-Control "'or:OrderConfirmation.Heading'" and save it as "OrdCnfHead"
		Then User verifies "'cache:OrdCnfHead'" "equal" "ORDER CONFIRMATION"
		When User fetch property "Displayed" of Web-Control "'or:OrderConfirmation.ShippingTab'" and save it as "ShipTab"
		Then User verifies "'cache:ShipTab'" is "true"
		When User fetch property "Displayed" of Web-Control "'or:OrderConfirmation.PaymentTab'" and save it as "PayTab"
		Then User verifies "'cache:PayTab'" is "true"
		When User fetch property "Text" of Web-Control "'or:OrderConfirmation.OrderCompleteMsg'" and save it as "OrdCmpltMsg"
		Then User verifies "'cache:OrdCmpltMsg'" "contain" "Your order on My Store is complete."
		When User save current URL as "userAccountPageURL"
		Then User verifies "'cache:userAccountPageURL'" "contain" "controller=order-confirmation"
		
	Examples:
	| email 													 | pwd 			|
	| hf_challenge_123456@hf123456.com | 12345678 |